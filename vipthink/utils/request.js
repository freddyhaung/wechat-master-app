/** 
 * 自定义post函数，返回Promise
 * +----------------------
 * @param {String}      url 接口网址
 * @param {arrayObject} data 要传的数组对象 例如: {name: '武当山道士', age: 32}
 * +-------------------
 * @return {Promise}    promise 返回promise供后续操作
 */
 function request(method, url, data,cookie) {
  var promise = new Promise((resolve, reject) => {
    
    var postData = data;
    //网络请求
    wx.request({
      url: url,
      data: postData,
      method: method,
      header: { 'content-type': 'application/x-www-form-urlencoded', ...cookie },
      success: function (res) {//服务器返回数据
      if (res.data.code == 200 || res.data.code == 201) {//res.data 为 后台返回数据，格式为{"data":{...}, "info":"成功", "status":1}, 后台规定：如果status为1,既是正确结果。可以根据自己业务逻辑来设定判断条件
          resolve(res.data);
        } else {//返回错误提示信息
          reject(res.data);
        }
      },
      error: function (e) {
        reject('网络出错');
      }
    })

  });
  return promise;
}

module.exports = {
  request: request
}
