const weixin = require('../../utils/request.js')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bgFlag:true,
    phone:'',
    smsCode:'',
    S:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    setTimeout(()=>{
      this.setData({
        bgFlag: false
      })
    },600)
    
  },
  getPhone:function(e){
    console.log(e.detail.value)
    let phone = e.detail.value;
    this.setData({
      phone:phone
    })
  },
  sendCode:function(){ // 发送验证码
    let phone = this.data.phone;
    if (!this._verPhone(phone)) {
      return false;
    }

    this.setData({
      S: 60
    })
    let timer = setInterval(() => {
      var S = this.data.S
      S--
      this.setData({
        S: S
      })
      if (this.data.S <= 0) {
        this.setData({
          S: 0
        })
        clearInterval(timer)
      }
    }, 1000)

    const url = app.globalData.API[0] + '/index/sms/sends';
    const data = {
      mobile: phone,
      type: -1
    }
    weixin.request('POST', url, data).then(res => {
      if (res.code === 200) {
        
        wx.showToast({
          title: '发送成功',
          icon: 'none'
        })
      
      }else{

        wx.showToast({
          title: res.msg,
          icon: 'none'
        })

      }

    }).catch(er => {

      wx.showToast({
        title: er.msg,
        icon: 'none'
      })

    })

  },
  getsmsCode:function(e) { // 获取验证码
    this.setData({
      smsCode:e.detail.value
    })
  },
  login:function(){ // 登录
    const phone = this.data.phone;
    const smsCode = this.data.smsCode;

    // console.log('获取到的code' + this.code);
    if (!this._verPhone(phone)) {
         return false;
    }

    if (smsCode == '') {
      wx.showToast({
        title: '验证码不能为空',
        icon: 'none'
      })
      return false;
    }

    const url = app.globalData.API[0] + '/index/account/login';
    const data = {
      mobile: phone,
      smsCode: smsCode,
      loginType: 2
    }

    weixin.request("POST",url,data).then(res => {
      const msg = res.msg;
      if (res.code === 200) {
        
        wx.showToast({
          title: '登录成功',
          icon: 'none'
        })
        let cookie = res.cookie;
        let userInfo = res.data

        app.globalData.cookie = cookie; //全局的cookie
        app.globalData.userInfo = userInfo;

        wx.setStorageSync('cookie', cookie);
        wx.setStorageSync('userInfo', userInfo);
       
        wx.redirectTo({
          url: '../userCenter/userCenter'
        })
       

      } else {
        
        wx.showToast({
          title: msg,
          icon: 'none'
        })

      }
    })

  },
  _verPhone(phone) { // 验证手机号
    if (phone == '') {
      wx.showToast({
        title: '手机不能为空',
        icon:'none'
      })
      return false;
    } else if (!/^1[3|4|5|6|7|8|9]\d{9}$/.test(phone)) {
      wx.showToast({
        title: '手机号码有误',
        icon: 'none'
      })
      return false;
    } else {
      return true;
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  }
})