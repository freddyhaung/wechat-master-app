
const weixin = require('../../utils/request.js')
const util = require('../../utils/util.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    courseDetail:{},
    liveId:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

    console.log(options.liveId)

    let liveId = options.liveId

    that.setData({
      liveId: liveId
    })
    getCourseDetail(that, liveId); //请求课程详情

  },
  /**
   * 我要请求参数
   */
  myLeave(){
    var that = this;
    let liveId = that.data.liveId
    wx.showModal({
      title: '提示',
      content: '你确认要请假码？',
      confirmColor:"#f7ab00",
      success: function (res) {
        if (res.confirm) {
          reqMyLeave(that, liveId)
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },


  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  }
})


/**
 * 请求我的合同详情
 * 
 */
function getCourseDetail(that, liveId) {

  const url = app.globalData.API[0] + '/api_index.php/index/my/courseDetail';

  const data = {
    liveId: liveId
  }

  const cookie = app.globalData.cookie || wx.getStorageSync('cookie')

  weixin.request('POST', url, data, cookie).then(res => {
    console.log(res);
    if (res.code === 200) {

        var courseDetail = res.data
         that.setData({
           courseDetail:courseDetail
         })
    } else if (res.code === 201) {

    }

  })

}

/**
 *  我要请假接口
 * 
 */
function reqMyLeave(that, liveId) {

  const url = app.globalData.API[0] + '/api_index.php/index/my/leave';

  const data = {
    liveId: liveId
  }

  const cookie = app.globalData.cookie || wx.getStorageSync('cookie')

  weixin.request('POST', url, data, cookie).then(res => {
    console.log(res);
    if (res.code === 200) {

      wx.showToast({
        title: res.msg,
        icon: 'none'
      })

    } else {

      wx.showToast({
        title: res.msg,
        icon: 'none'
      })

    }

  })

}