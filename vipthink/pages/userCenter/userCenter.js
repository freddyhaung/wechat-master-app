const weixin = require('../../utils/request.js')
const util = require('../../utils/util.js')
const app = getApp()
var sliderWidth = 120; // 需要设置slider的宽度，用于计算中间位置
var page = 1
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: ["学习进度(4/90)", "我的合同"],
    userInfo:[],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,
    courseData:{},
    courseDataFlag:false,
    treatyData:[],
    treatyDataFlag:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });
    
    var userInfo = wx.getStorageSync('userInfo')||[]; //在缓存获取用户的信息
    this.setData({
      userInfo:userInfo
    })

    //获取 我的课程接口
    getmyCourse(that);

    //获取 合同列表接口
    getOrder(page,that)

  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  /**
   * 进入我的课程详情
   */
  toCourseDetails:function(e){
   
    var liveId = e.currentTarget.dataset.liveid
    wx.navigateTo({
      url: '../courseDetail/courseDetail?liveId=' + liveId,
    })
  },
   /**
   * 进入我的合同详情
   */

  toContractDetail:function(e){
    
    var orderId = e.currentTarget.dataset.orderid
    wx.navigateTo({
      url: '../contract/contract?orderId=' + orderId,
    })
  },
    /**
   * 进入编辑个人资料
   */

  toedit:function(){
    wx.navigateTo({
      url: '../edit/edit'
    })
  },
   /**
   * 进入查看课程介绍
   */

  toweb_system:function(){
    wx.navigateTo({
      url: '../web_system/web_system'
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    that.setData({
      courseData:{},
      treatyData:[]
    })

    getmyCourse(that);
    getOrder(1, that);

  },

  /**
   * 页面上拉触底事件的处理函数
   * 
   */
  onReachBottom: function () {
    var that = this;
    page ++ ;
    getOrder(page, that);
  }
})

/**
 * 我的课程（学习进度）
 * 
 */
function getmyCourse(that){

  const url = app.globalData.API[0] + '/api_index.php/index/my/courseLists';
  const data = {
    
  }
  const cookie = app.globalData.cookie || wx.getStorageSync('cookie')
  
  weixin.request('POST', url, data,cookie).then(res => {
    console.log("我的课程",res)
    var tabs = that.data.tabs;
    if (res.code === 200) {
      var dataLength = res.data.length; //我的课程总数
      for (let i = 0; i < dataLength; i++) {
        res.data[i].classDate = util.formatTime(new Date(res.data[i].classDate * 1000),1)
        res.data[i].startTime = util.formatTime(new Date(res.data[i].startTime * 1000),2)
        res.data[i].endTime = util.formatTime(new Date(res.data[i].endTime * 1000),2)
        // if(){
        //   res.data[i].status  = 2
        // }
      }

      
      tabs[0] = `学习进度(4)`;
      that.setData({
        courseData: res.data,
        courseDataFlag: true,
        tabs: tabs
      })
    }else{
      tabs[0] = `学习进度`;
      that.setData({
        courseDataFlag: false,
        tabs: tabs
      })

    }
  }).catch(er=>{
   
    if (er.code == 401) {
      
      try {
        wx.removeStorageSync('userInfo');
        wx.removeStorageSync('cookie');
      } catch (e) {
        console.log(e)
      }

      wx.reLaunch({
        url: '../login/login'
      })

    }

  })
   
}
/**
 * 请求我的合同
 * 
 */
function getOrder(page,that) {

  const url = app.globalData.API[0] + '/api_index.php/index/my/orderLists';
 
  const data = {
    page: page,
    limit: 10
  }

  const cookie = app.globalData.cookie || wx.getStorageSync('cookie')

  weixin.request('POST', url, data,cookie).then(res => {
    if (res.code === 200) {
      console.log('我的合同数据',res)
      console.log(res.data)
      if(res.data){
        for(let i = 0 ;i<res.data.length;i++){
          res.data[i].startTime = util.formatTime(new Date(res.data[i].startTime*1000),2)
          res.data[i].endTime = util.formatTime(new Date(res.data[i].endTime * 1000),2)
        }
        //var treatyData = that.data.treatyData.concat(res.data)
        var treatyData = res.data
        that.setData({
          treatyData: treatyData,
          treatyDataFlag: true
        })

      }else{

        that.setData({
          treatyDataFlag:false
        })

      }
      console.log(that.data.treatyData)
    }else if(res.code === 201 ){
      console.log(res)
    }

  }).catch(er => {

    if (er.code == 401) {
      console.log(401)
      try {
        wx.removeStorageSync('userInfo');
        wx.removeStorageSync('cookie');
      } catch (e) {
        console.log(e)
      }

      wx.reLaunch({
        url: '../login/login'
      })

    }

  })

}
