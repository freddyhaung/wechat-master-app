
const weixin = require('../../utils/request.js')

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:{},
    date:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var that = this;

    getInfo(that)

  },

  /**
   * 显示性别选择器
   */
  showSex:function(){
    var that = this;
    wx.showActionSheet({
      itemList: ['男', '女'],
      success: function (res) {

        console.log(res.tapIndex)
        
        var userInfo = that.data.userInfo;
        if(res.tapIndex == 0){
         
          userInfo.sex = 2;

        } else if (res.tapIndex == 1){
         
          userInfo.sex = 1;

        }
        that.setData({
          userInfo: userInfo
        })
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  },
 /**
   * 显示年龄选择器
   */
  bindDateChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    var that = this;
    var userInfo = that.data.userInfo;
   
    userInfo.birthday = e.detail.value;
    
    that.setData({
      userInfo: userInfo
    })
  },
  /**
   * 显示宝贝关系
   */
  showRelation:function(){
    var that = this;
    let itemList = ['爸爸', '妈妈', '爷爷', '奶奶', '外公', '外婆']
    wx.showActionSheet({
      itemList: itemList,
      success: function (res) {

        console.log(res.tapIndex)
        const tapIndex = res.tapIndex;
        var userInfo = that.data.userInfo;

        itemList.forEach(function(item,index,arr){
          if (tapIndex == index) {
            userInfo.relation = item;
          }
          that.setData({
            userInfo: userInfo
          })
        })

      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  },

  formSubmit: function (e) {
    console.log('form发生了submit事件，携带数据为：', e.detail.value)
    var that = this;
    const userName = e.detail.value.userName
    const nickName = e.detail.value.nickName
    const sex = e.detail.value.sex
    const birthday = e.detail.value.birthday
    const school = e.detail.value.school
    const parentName = e.detail.value.parentName
    const relation = e.detail.value.relation
    const address = e.detail.value.address
    const estate = e.detail.value.estate
    console.log(userName)
    if (sex == '男'){
      var sexNum = 1
    }else{
      var sexNum = 2
    }
    const data = {
      name: userName,
      nickName: nickName,
      sex: sexNum,
      birthday: birthday,
      school: school,
      parentName: parentName,
      relation: relation,
      address: address,
      estate: estate
    }

    editInfo(that,data)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

})



/**
 * 请求完善个人信息
 * 
 */
function editInfo(that, data) {

  const url = app.globalData.API[0] + '/api_index.php/index/my/editInfo';

console.log(data)

  const cookie = app.globalData.cookie || wx.getStorageSync('cookie')

  weixin.request('POST', url, data, cookie).then(res => {
    if (res.code === 200) {

      console.log(res)
      getInfo(that);

      wx.showToast({
        title: res.msg,
        icon: 'none'
      })

      wx.navigateTo({
        url: '../userCenter/userCenter'
      })

    } else {

      wx.showToast({
        title: res.msg,
        icon: 'none'
      })
    }

  })
}


/**
 * 我的个人信息
 * 
 */
function getInfo(that) {

  const url = app.globalData.API[0] + '/index/my/info';

  const data = {

  }

  const cookie = app.globalData.cookie || wx.getStorageSync('cookie')

  weixin.request('POST', url, data, cookie).then(res => {
    if (res.code === 200) {

      app.globalData.userInfo = res.data;

    
      wx.setStorageSync('userInfo', res.data);

      var userInfo = res.data || wx.getStorageSync('userInfo'); //在缓存获取用户的信息

      that.setData({
        userInfo: userInfo
      })

    } else if (res.code === 401) {

      wx.navigateTo({
        url: '../login/login',
      })

    } else {

      wx: wx.showToast({
        title: res.msg,
        icon: 'none'
      })

    }

  })

}