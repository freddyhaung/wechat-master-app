
const weixin = require('../../utils/request.js')
const util = require('../../utils/util.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderData:{}  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   var that = this;

   var orderId = options.orderId;

   getOrderDetail(that, orderId); //请求合同详情

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  }
})


/**
 * 请求我的合同详情
 * 
 */
function getOrderDetail(that, orderId){

  const url = app.globalData.API[0] + '/api_index.php/index/my/orderDetail';

  const data = {
    orderId: orderId
  }

  const cookie = app.globalData.cookie || wx.getStorageSync('cookie')

  weixin.request('POST', url, data, cookie).then(res => {
    if (res.code === 200) {

     console.log(res)
    
       res.data.classDate = util.formatTime(new Date(res.data.classDate * 1000), 1)
       res.data.startTime = util.formatTime(new Date(res.data.startTime * 1000),2)
       res.data.endTime = util.formatTime(new Date(res.data.endTime * 1000),2)
       res.data.createTime = util.formatTime(new Date(res.data.createTime * 1000), 1)
       res.data.payTime = util.formatTime(new Date(res.data.payTime * 1000), 1)
   
     that.setData({
       orderData:res.data
     })

    } else if (res.code === 201) {
     
    }

  })

}